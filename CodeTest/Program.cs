﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;

namespace CodeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Code Test ...");

            TestMemoizedFunction();
            TestBinary();
        }
        
        /// <summary>
        /// Test Memoized IsPrime
        /// </summary>
        private static void TestMemoizedFunction()
        {
            Console.WriteLine("Memoize function testing....");
            //create memoized IsPrime
            var memoizedIsPrime = Memoize<int, bool>(IsPrime);

            //Use memoized IsPrime
            Console.WriteLine("Enter a number to check if it is a prime number:");
            var value = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"The number you entered is {(memoizedIsPrime(value) ? "a" : "not a")} Prime Number");
            Console.ReadLine();
        }
        /// <summary>
        /// Returns true is value is prime, false otherwise
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsPrime(int value)
        {
            if (value <= 2) return 2 == 0;
            for (int v = 2; v <= value / 2; v++)
            {
                if (value % v == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns a memoized version of the given function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func"></param>
        /// <returns></returns>
        private static Func<T, TResult> Memoize<T, TResult>(Func<T, TResult> func)
        {
            var cache = new Dictionary<T, TResult>();
            return a =>
            {
                TResult value;
                if (cache.TryGetValue(a, out value))
                {
                    return value;
                }
                value = func(a);
                cache.Add(a, value);
                return value;
            };
        }

        private static void TestBinary()
        {
            Console.WriteLine("Bianry Search testing....");

            var array = new int[] { 1, 2, 4, 7, 9, 21, 27, 78 };
            Console.WriteLine("Enter a number to search...");

            var value = Convert.ToInt32(Console.ReadLine());
            var index = BinarySearch(array, value);
            if (index > 0)
            {
                Console.WriteLine($"{value} found at index {index}");
            }
            else
            {
                Console.WriteLine($"{value} not found");
            }
            Console.ReadLine();
        }
        /// <summary>
        /// Searches the given key from the given array
        /// </summary>
        /// <param name="array"></param>
        /// <param name="key"></param>
        /// <param name="isSorted"></param>
        /// <returns></returns>
        private static int BinarySearch(int[]array, int key, bool isSorted = true)
        {
            if (!isSorted)
            {
               Array.Sort(array);
            }

            int low = 0, high = array.Length-1;
            while (low < high)
            {
                var mid = (low + high )/ 2;
                if(key == array[mid])
                {
                    return ++mid;
                }
                else if(key < array[mid])
                {
                    high = mid - 1;
                }
                else
                {
                    low = mid + 1;
                }
            }
            return -1;

        }
    }
}
